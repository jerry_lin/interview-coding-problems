#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<sstream>

/*
http://www.mitbbs.com/mitbbs_article_t.php?board=JobHunting&gid=32260339&ftype=0
String s is called unique if all the characters of s are different.

String s2 is producible from string s1, if we can remove some characters of
s1 to obtain s2.

String s1 is more beautiful than string s2 if length of s1 is more than
length of s2 or they have equal length and s1 is lexicographically greater
than s2.

Given a string s you have to find the most beautiful unique string that is
producible from s.

Input:

First line of input comes a string s having no more than 1,000,000(10^6)
characters. all the characters of s are lowercase english letters.

Output:

Print the most beautiful unique string that is producable from s

Sample Input:

babab

Sample Output:

ba

Explanation

In the above test case all unique strings that are producible from s are "ab
" and "ba" and "ba" is more beautiful than "ab".
 */

using namespace std;

#define N 256

class Solution {
	public:
	string findMostBeuatiful(string s)
	{
		string ans;
		int len = s.size();
		if(len<=1) return s;

		int count[N]={0};
		bool used[N]={false};

		for(int i=0; i<len; i++)
			count[s[i]]++;
		
		vector<char> res;
		for(int i=0; i<len; i++)
		{
			char ch=s[i];
			count[ch]--;
			if(used[ch]) continue;
			
			while(res.size()>0)
			{
				char ch2=res.back();
				if(ch2<ch && count[ch2]>0)	
				{
					res.pop_back();
					used[ch2]=false;
				}
				else
					break;
			}
			res.push_back(ch);
			used[ch]=true;
		}

		stringstream ss;
		for(int i=0; i<res.size(); i++)	ss << res[i];
		return ss.str();
	}
};

int main()
{
	Solution sol;

	string input="nlhthgrfdnnlprjtecpdrthigjoqdejsfkasoctjijaoebqlrgaiakfsbljmpibkidjsrtkgrdnqsknbarpabgokbsrfhmeklrle";

	string output="tsocrpkijgdqnbafhmle";

	string ans=sol.findMostBeuatiful(input);
	cout << ans << endl;

	if(ans==output) cout << "Correct" << endl;
	else cout<< "Wrong" << endl;

	return 0;
}
